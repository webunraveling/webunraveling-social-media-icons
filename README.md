# WebUnraveling Social Media Icons Widget 
This WordPress plugin adds a simple widget that displays social media icons that link to the social media account you provide. It also will display an about us button that leads to the URL of your choice. Just provide the URL and the icon will appear.
