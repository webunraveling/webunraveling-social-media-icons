<?php
/*
Plugin Name:  WebUnraveling Social Media Icons
Description:  A widget that displays social media icons based on the URLs provided. 
Plugin URI:   https://bitbucket.org/webunraveling/webunraveling-social-media-icons
Version:      1.0
Author:       Jason Raveling
Author URI:   https://webunraveling.com
*/

// Register the widget stylesheets
add_action( 'wp_enqueue_scripts', 'wunrav_widgets_register_css' ); // front-end
add_action( 'admin_enqueue_scripts', 'wunrav_widgets_register_admin_css' ); // admin

function wunrav_widgets_register_css() {
    wp_register_style(
        'wunrav-social-media-icons',
        plugins_url( '/webunraveling-social-media-icons/style/css/wunrav-social-media-icons.css' ),
        array(),
        filemtime( dirname(__FILE__) . '/style/css/wunrav-social-media-icons.css' )
    );
   
    wp_enqueue_style( 'wunrav-social-media-icons' );
}

function wunrav_widgets_register_admin_css() {
    wp_register_style(
        'wunrav-social-media-icons-admin',
        plugins_url( '/webunraveling-social-media-icons/style/css/wunrav-social-media-icons-admin.css' ),
        array(),
        filemtime( dirname(__FILE__) . '/style/css/wunrav-social-media-icons-admin.css' )
    );
    
    wp_enqueue_style( 'wunrav-social-media-icons-admin' );
}

// Register and get the widget
require_once( dirname(__file__) . '/include/widget.php' );

?>
